# Webtechnologie 2 #

Link naar bitbucket pagina :[https://bitbucket.org/alextozzi/webtech-2-alexandre-tozzi](https://bitbucket.org/alextozzi/webtech-2-alexandre-tozzi)

[logo]: http://sdtimes.com/wp-content/uploads/2014/08/0826.sdt-git-21.jpg "Logo"

##Lab 1 - Version control with GIT ##

- [https://github.com/Mathijsvdb/GITtowork](https://github.com/Mathijsvdb/GITtowork) 

### De bedoeling van de eerste opdracht was om een repository op github aan te maken en hier een klein project maken met vier personen. Nadien hebben we we onze naam  en voornaam verstuurd naar één repository door middel van pull request. ###

Basis begrippen van GIT:

	git clone: maakt een kopie van je repository , dit gebeurd lokaal.
    git pull: haalt de versie op die je het laatst op Github of Bitbucket hebt geplaatst
    git fetch: hetzelfde als pull, maar gaat bestanden niet mergen.
    git add: Dit command laat je nieuwe of gewijzigde bestanden naar je repository op Github/Bitbucket pushen.
    git commit -m "message": na het toevoegen van je bestanden moet je voorzien van een bericht, dit bericht zal dan te zien zijn op bitbucket/github na het pushen. Zo Weet iedereen wat je gedaan hebt.
    git push: Nu kan je eindelijk je committed bestanden naar je repository pushen op GitHub/Bitbucket.



### Branches ###

#### Branches worden gebruikt om verschillende features te ontwikkelen in geïsoleerde omgevingen, los van elkaar. De master branch is de basis of standaard branch wanneer je een nieuwe repository aanmaakt. Je maakt nieuwe branches aan wanneer je nieuwe toevoegingen ontwikkelt en voegt ze samen (merge) met de master branch wanneer je klaar bent.   

	git checkout test: ga naar de branch test
    git checkout master: ga terug naar de master branch
    git branch -d test: verwijder de test branch
    git push origin <branch>: jouw branch synchroniseren met de centrale repository



### Update & merge ###

	git pull : dit command laat je je lokale repository updaten naar de laatste versie.
    git merge <branch> : dit command laat je toe om je branches te mergen , mergen betekend samenvoegen.


### Forking ###

Forking bied je de mogelijkheid om  een kopie te nemen van een repository en komt bij jou Github/Bitbucket account bij. Na dat dit gekopieerd is moet je wel de repository klonen naar je lokale machine.  

### Pull requests ###
Wanneer je een repository tegen komt en je ziet een fout of een gemakkelijkere oplossing, dan kan je dit oplossen door deze repository te forken (zoals uitgelegd hierboven) en klonen. Wanneer je dit oplost, dan kan je een pull request sturen naar de gebruiker van de oorspronkelijke repository. 

##Lab 2 - mastering CSS animations ##

### In week les 2 leerde we objecten te animeren via CSS. Hier onder zal uitgelegd worden wat transitions, transformations en animations zijn. ###

#### Transitions ####
Transition is de overgang van bv + naar een X of een button met een hover dan veranderd de kleur. Het nadeel van het gebruik van een transition is dat het niet automatisch afgespeeld wordt, je moet het altijd zelf triggeren in CSS bv met een hover.

**Voorbeeld****Transition** **all** **0.2s** **ease****-in;**

#### Transformations ####

Bij transformations veranderd de look van een element, het heeft ook betere performance.

**voorbeeld** **Transform****:** **translateX****(200px)** **scale(2);**

##### 2D Transformation#####
	
**Translate(x,y)**
Met deze methode gaat een element bewegen van zijn huidige positie naar de positie die je hebt opgegeven. Met de x waarde ga je van links naar rechts en met de Y waarde ga je naar boven en beneden bewegen.

**rotate(angle)**
De rotatie methode biedt je de mogelijkheid om het element te draaien. Het aantal graden bepaal je met een angle.

**scale(x,y)**
Met de scale methode ga je het element vergroten of verkleinen. Scalen via de x en y as, X om de breedte aan te passen en y om de hoogte aan te passen.

**Skew(x,y)**
Skew laat je element kantelen rond de x en y-as. Men kan kantelen via skewX horizontaal en skewY verticaal kantelen.

##### 3D Transformation#####

**rotateX en rotate(y)**
Je roteert via de x as en de y as.

**perspective(value)**
Hier wordt het perspectief bepaald van het object. Met andere woorden, de afstand tussen element en de gebruiker wordt hier beïnvloed.

!!! De perspective methode heeft alleen maar invloed op 3D transformations(rotateX & Y);



#### Animations ####
Animatie geeft je de mogelijkheid om een object te animeren door gebruik van keyframes, in tegenstelling tot de transition kan het object wel automatisch afgespeeld worden in een loop.  


**voorbeeld:** 
**Animations** **:** **translate()** **rotate()** **scale()** **skew()** **matrix();**

**–webkit-animation:** **move** **10s** **infinite;**
**@-webkit-keyframes** **move{ 0% {border-radius:0}** **20%** **{transform translatex(20px)}** **40%** **{transform : translatey(40px;**
**100%** **{border-radius:50%}}**


Zoals we zien , moet je eerst een declaratie opvragen waarbij je de functie naam "move" de duur en wat voor loop het moet zijn. We zien ook dat ‘webkit’ altijd voor animations staat. Dit moet gedaan worden omdat niet alle browsers animations kennen.

[Leuke link met verschillende animaties](http://daneden.github.io/animate.css/) 
 
##Lab 3 - Advanced Javascript##

### In les week 3 kwamen we in contact met ons eerste framework, we kregen hier de opdracht een ‘to do list’ applicatie te maken. Dit mocht niet in jquery maar wel in javascript geschreven worden.###

#### Prototypes ####

Prototypes zijn flexibel. Met prototypes kan je je eigen mini framework schrijven zonder het hele jquery framework in te laden. Dit heeft invloed op de laadtijd, namelijk een vermindering. Dit is goed als men mobiele applicaties maakt.


##### Objects #####

**Objecten** **zijn** **data** **types** **in** **javascript.**

- Boolean is een object.
- Number is een object
- String is een object.
- Datum, arrays en functies zijn objecten.

**Voorbeeld**
	Var banaan = new Object();
	banaan.kleur = "geel";
	banaan.grote = 10;

	Banaan.toon = function(){
		console.log(this.kleur + " is de kleur van de banaan" + " met als grote " + this.grote + " cm") ;
	} 


##Lab 4 - Building an app prototype with APIs##

### Deze week leerde we onze eerste applicatie maken met open data. De bedoeling was om een weerapplicatie te bouwen en deze moest gebruik maken van forecast API en HTML5 geolocations voor de huidige locatie.###

### API ###
API staat voor Application Programming Interface en is eigenlijk een soort van bibliotheek waar data inzit. Deze data kunnen we opvragen. Voor deze taak gebruiken we forecast API.

**!Waarschuwing!** Je kan maar maximum 1000 keer per dag deze data opvragen. Wil je meer dan 1000 keer deze data opvragen dan moet je betalen.


### Geolocation###
Met geolocation kan men in HTML5 de geografische positie van de gebruiker weergeven. Deze coördinaten kan je dan gebruiken in je forecast API of in Google Maps.

**Voorbeeld**

	function getLocation() {
    if ( navigator .geolocation ){
        navigator .geolocation.getCurrentPosition ( showPosition );
    } else {
    alert ( "Geolocation is not supported by this browser!" );
    }
	}

	//locatie weergeven in de div demo
	function showPosition(position) {
    document. getElementById ( "demo" ) .innerHTML = "Latitude: " + position.coords.latitude + "
    Longitude: " + position.coords.longitude ;
	}

### Localstorage ###
Omdat we met een limiet zitten van 1000 keer per dag de data opvragen kan men dit oplossen met behulp van een localstorage. Dit doet is het cachen van data voor een bepaalde tijd.

**Voorbeeld**

	localstorage.setItem ( "weather" , response );
	var weather = localstorage.getItem ( "weather");

### AJAX ###
AJAX staat voor Asynchronous JavaScript And XML. Ajax zorgt ervoor dat de pagina niet gerefresht moet worden wanneer het nieuwe data ophaalt. 

**Voorbeeld**

	$ .ajax({
    url: "http://urlvanapi" ,
    success: function ( response ){ //do stuff here }
	});

### JSON & JSONP
JSON staat voor javaScript Object Notation. JSON is een object dat terugkomt van een API. Het wordt gebruikt om data uit te wisselen tussen server en de webapplicatie.

JSONP staat dan voor JSON zoals hier boven beschreven maar met Padding.
JSONP is een manier om met JavaScript Cross-Origin Requests te doen. Dat wil zeggen om requests te doen naar een server binnen een ander (sub)domein dan het huidige. Dit is namelijk met een standaard XMLHttpRequest niet mogelijk wegens veiligheidsoverwegingen. In nieuwe browsers is dit probleem opgelost door de implementatie van Cross-origin resource sharing (CORS). De implementatie  van CORS vraagt echter om complexere aanpassingen van je webservice dan JSONP en wordt bovendien niet ondersteunt door oude browsers en veel mobiele browsers. Veel webservices maken daarom nog steeds gebruik van JSONP om Cross-Origin Requests af te handelen.


**Voorbeeld**
	
	$ .ajax({
    url: "http://urlvanapi" ,
    dataType: jsonp ,
    success: function ( response ){ //do stuff here }
	});

##Lab 5 - realtime apps with node.js and web sockets##

### Wat is node JS ?###
Node.js is een softwareplatform waarop men applicaties kan ontwikkelen en draaien. Die applicaties worden geschreven in JavaScript, maar in tegenstelling tot veel andere Javascript-toepassingen, worden die niet uitgevoerd in een webbrowser, maar in de JavaScript-Engine van Node.js zelf, die op de server draait. De applicaties kunnen draaien op elke computer waarop de Node.js runtime is geïnstalleerd, dat kan Windows, Mac OS X of Linux zijn.

### Functies van node JS ###
Node.js bevat een ingebouwde HTTP-server, waardoor het mogelijk is een webserver te draaien zonder Apache of Lighttpd. Daardoor biedt Node.js een alternatieve manier van Server-side scripting, vergeleken met andere platformen zoals Java EE, ASP.NET, Ruby On Rails of een traditionele webserver met CGI-modules voor diverse programmeertalen zoals PHP, Perl of Python.

### Non-blocking IO ###
Beter bekend als asynchronous IO en is de afkorting voor non-blocking input/output. Dit wil zeggen dat terwijl er nog een input of output wordt verwerkt, andere processen blijven voortgaan tot de huidige actie voltooid is. Dit betekent dat we veel meer aanvragen tegelijkertijd kunnen accepteren.

### Synchronous vs. asynchronous ###
Synchronous IO is het tegenovergestelde van non-blocking IO. Bij synchronous IO moet de huidige input of output helemaal verwerkt zijn voor het volgende proces verwerkt kan worden. Dit betekent dat het programma kan blokkeren wanneer de communicatie in volle gang is.

### Web sockets ###
Web Sockets laten real-time 2-richtingsverkeer communicatie tussen server en client toe, een vorm van streaming dus. WS laten zeer snelle en onmiddellijke communicatie toe, vb. chat.

Tot nu toe is het verkeer één-richting: de browser stuurt een verzoek en de server antwoordt. De server kan geen initiatief nemen. Met Web Sockets wel, de server kan ongevraagd gegevens doorsturen via de socket: push.


### Installatie Node.js app ###
	
**Installeren** **Node.js**
Eerst moet men node.js downloaden en installeren via de website.

**installeer** **express** **global**
Om express global te installeren moet je in de terminal dit command typen.

	$npm install express -g

**gernereer een express app**

Ga eerst via de terminal naar de gewenste map waar je de applicatie wil installeren. Dit kan met behulp van cd, dit staat voor change directory.


Vervolgens moet men de dependenties installeren. Dit kan door het volgende commando.

	
	$npm install

De applicatie gaat nu al werken maar om het op te starten moeten we eerst nog een commando typen en dat is.

	node app.js

### Package.json###
In dit bestand vinden we alle data en dependenties om je applicatie te laten draaien. Als men jade wil gebruiken moet men de dependenties toevoegen aan deze package.

### Jade ###
Jade is een default template engine voor node. Dit is sneller en korter geschreven dan HTML, daarom is het interessanter om jade te gebruiken in plaats van HTML. Je kan spaties en tabs gebruiken maar opgelet. Eens men tabs gebruikt moet men dit blijven gebruiken anders zal de code niet werken.

### Nodemon ###
Nodemon gaat de applicatie automatisch updaten als er iets gewijzigd wordt in een van de bestanden. Om nodemon te installeren moet men volgende commando’s typen.

	//global install
	npm install nodemon -g

	//app opstarten met nodemon
	nodemon app.js

## Lab 6 - Angular.js##

### Wat is Angular.js?###
AngularJS, vaak aangeduid als Angular, is een opensource webapplicatie framework dat wordt onderhouden door Google en een collectief van individuele ontwikkelaars en bedrijven die bezig zijn de mogelijkheden voor het ontwikkelen van Single Page Applications (SPA) te verbeteren. Het doel is het vereenvoudigen van zowel de ontwikkeling als het testen van dergelijke applicaties door het aanbieden van een framework voor client-side model-view-controller (MVC)-architectuur, samen met componenten die gewoonlijk worden gebruikt in Rich Internet Applications.

Het framework werkt door eerst de HTML-pagina te lezen, waarin aanvullende specifieke HTML-attributen zijn opgenomen. Die attributen worden geïnterpreteerd als directieven (Engels: directives)[1] die ervoor zorgen dat Angular invoer- of uitvoercomponenten van de pagina koppelt aan een model dat wordt weergegeven door middel van standaard JavaScript-variabelen. De waarden van die JavaScript-variabelen kunnen worden ingesteld binnen de code, of worden opgehaald uit statische of dynamische JSON-dataobjecten.


### Belangrijke directives###

**ng-app**

	declareert het root-element (het hoogste element in de structuur) van een Angular-applicatie, waaronder directives gebruikt kunnen worden om data-koppelingen te declareren en gedrag te definiëren.
	
**ng-bind**

		        stelt de tekst van een DOM-element in op de waarde van een expressie. <span ng-bind="naam"></span> zal bijvoorbeeld de waarde van de variabele 'naam' binnen het span-element laten zien. Alle veranderingen in de variabele ‘naam’ in de scope van de applicatie worden onmiddellijk in het DOM weergegeven.

**ng-model**

	vergelijkbaar met ng-bind, maar deze directive ontwikkelt een data-koppeling in twee richtingen tussen de view en de scope.
**ng-model-options**

	maakt het mogelijk in te stellen hoe wijzigingen in het model worden aangebracht.
	
**ng-class**

	maakt het mogelijk CSS-klasse-attributen dynamisch te koppelen aan een HTML-element. Dit gebeurt door middel van een data-koppeling van een expressie, die de toe te voegen CSS-klassen vertegenwoordigt.
	
**ng-controller**

	koppelt een JavaScript-controller-klasse aan de view. Dit is een cruciaal onderdeel van de manier waarop Angular het model-view-controller ontwerppatroon implementeert. Controller-klassen bevatten de bedrijfslogica achter de applicatie, zij voorzien de scope van functies en waarden.
	
**ng-repeat**

	instantieert een element eenmaal per item uit een verzameling.
	
**ng-show & ng-hide**

	toont of verbergt een element op basis van een conditie, afhankelijk van de waarde van een boolean-expressie. Het tonen en verbergen gebeurt door het instellen van de CSS-presentatiestijl.
	
**ng-switch**

	    instantieert een template uit een verzameling keuzemogelijkheden op basis van een conditie, afhankelijk van de waarde van een expressie.
	
**ng-view**

	de directive die verantwoordelijk is voor het behandelen van routes die worden aangegeven door URL's. Het definiëren van routes zorgt ervoor dat templates worden weergegeven, die worden aangestuurd door gespecificeerde controllers.
	
**ng-if**

	deze directive zorgt voor het verwijderen of opnieuw tonen van een deel van het DOM, gebaseerd op een expressie die de waarde waar ('true') of onwaar ('false') kan aannemen. Wanneer de expressie onwaar is, wordt het element uit het DOM verwijderd. Wanneer de expressie waar is, wordt een exemplaar van het element (opnieuw) ingevoegd.


## Lab 7 - SASS + BEM##

###Sass is een extensie van CSS en maakt gebruik van variabelen, nesting, mixins, inheritance en operators om de stylesheet zo ordelijk mogelijk te maken. Het gevolg hiervan is dat je minder code moet schrijven en je stylesheet wordt ook veel overzichtelijker.###

#### Installatie sass####
Om sass te installeren moet men eerst Ruby installeren op Windows. Dit kan je doen door naar de website te gaan en de installer.exe te downloaden en te installeren.

Vervolgens gaat men enkele commands uitvoeren in de nieuwe Ruby prompt


	gem install sass

	//gebruik dit als je een error message krijgt
	sudo gem install sass

	//check of je sass zeker geïnstalleerd hebt door de versie op te vragen
	sass -v

#### Mappenstructuur####
De standaard mappenstructuur die we gebruiken voor sass zijn opgesplitst zodat we een betere structuur in ons project krijgen. We vinden hier files met underscores terug , deze worden niet automatisch gecompileerd. Je kan dit vergelijken als een soort van include file in PHP.


	/css
    	style.css
	/scss
    	/partials
    	_nav.scss
    _	footer.scss
	style.scss
	_base.scss

Als men een file met een onderscore wilt importeren moet dit in style.scss gedaan worden. Let wel op dat je de underscores en de scss extentie weglaat.

	@import 'base';
	@import 'partials/nav';
	@import 'partials/footer';

#### Compilteren####
Wanneer men handmatig wil compileren van een scss bestand naar CSS bestand moet dit met het volgende command gedaan worden in je terminal.
	
	sass style.scss ../css/style.css
Als men bestanden automatisch wil compileren naar een CSS bestand moet je volgende command gebruiken.

	sass --watch style.scss:../css/style.css

	//als je de css ineens wilt minifien
	sass --watch style.scss:../css/style.css --style compressed


#### variabelen###
Variabel is een manier om informatie op te slaan en te hergebruiken. Dat kunnen kleuren, lettertypen, enz... zijn. 
Als men het $ symbool gebruikt zal de variabele aangemaakt worden. 


**Voorbeeld**

	$font: Helvetica, sans-serif ;
	$primaire-kleur: #f7f7f7 ;

	body {
    font-family: $font ;
    color: $primaire-kleur ;
	}

#### Nesting####
Met sass kan men CSS selectors nesten.

**Voorbeeld**

	nav {
    ul {
        margin: 0 ;
        padding: 0 ;
        list-style: none ;
    }
    a {
        display: block ;
        padding: 15px ;
        text-decoration: none ;
     }
	}

####Mixins####
Met Mixins kan je een groep CSS declaraties maken en hergebruiken voor een website.  

**Voorbeeld**

	@mixin border-radius ( $radius ) {
    -webkit-border-radius: $radius ;
    -moz-border-radius: $radius ;
    -ms-border-radius: $radius ;
    border-radius: $radius ;
	}

	//include your mixin
	.rounded { @include border-radius ( 10px ); }

#### Inheritance###
Met de @extend functie kan je een set van CSS eigenschappen meegeven van een selector naar een andere.

**Voorbeeld**

	.melding {
    border: 1px solid #fff ;
    padding: 15px ;
    color: #000 ;
	}

	.success {
    @extend .message ;
    border-color: green ;
	}

	.error {
    @extend .message ;
    border-color: red ;
	}

	.warning {
    @extend .message ;
    border-color: yellow ;
	}


#### Bourbon.io####
Bourbon is een simpele mixin libary voor sass. Het kan gemakkelijk gebruikt worden en je moet zelfs een pak minder code schrijven. Om bourbon te installeren moet men volgende commando typen.
	
	gem install bourbon
Vervolgens moet men bourbon installeren in de stylesheet map van je project. Dit kan je doen via de CD command. en dan type je volgend commando.

	bourbon install
Laatste stap is de mixins bourbon importeren in de style.scss

	@import 'bourbon/bourbon';

Meer informatie over bourbon vindt men [hier!](http://bourbon.io/docs/)

## Lab 8 - GULP.JS

### Wat is gulp.js? ###
Gulp Js is een soort van streaming build systeem, met behulp van node's stream file manipulation wordt alles gedaanin het geheugen. Een bestand wordt niet geschreven tot je hem vertelt dat te doen.

### Wat doet gulp ? ###
- merge and minify CSS files####
- compress images
- remove unused CSS

###Installatie voor gulp ###
Eerst moet men gulp globaal installeren in je command prompt. 

	$ npm install gulp -g

Vervolgens ga je Gulp lokaal in je project dev dependencies installeren. Let wel op dat er een package.json file al aangemaakt is. Dit doen we met de volgende code.

	$ npm init

	$ npm install gulp --save-dev

### installatie van de gulp plugins ###

	Sass compile (gulp-ruby-sass)
	Autoprefixer (gulp-autoprefixer)
	Minify CSS (gulp-minify-css)
	JSHint (gulp-jshint)
	Concatenation (gulp-concat)
	Uglify (gulp-uglify)
	Compress images (gulp-imagemin)
	LiveReload (gulp-livereload)
	Caching of images so only changed images are compressed (gulp-cache)
	Notify of changes (gulp-notify)
	Clean files for a clean build (del)

Als je al deze plug-ins in 1 keer wil installeren kan men gebruik maken van dit command.

	$ npm install gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev

### Setup gulpfile.js##

Vervolgens wordt er van ons gevraagd dat er een bestand aangemaakt wordt genaamd gulpfile.js. Deze file moet zo geconfigureerd worden.

	// Load plugins
	var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');
 
	// Styles
	gulp.task('styles', function() {
	return gulp.src('src/styles/main.scss')
    .pipe(sass({ style: 'expanded', }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('dist/styles'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/styles'))
    .pipe(notify({ message: 'Styles task complete' }));
	});
 
	// Scripts
	gulp.task('scripts', function() {
	return gulp.src('src/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist/scripts'))
    .pipe(notify({ message: 'Scripts task complete' }));
	});
 
	// Images
	gulp.task('images', function() {
	return gulp.src('src/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/images'))
    .pipe(notify({ message: 'Images task complete' }));
	});
 
	// Clean
	gulp.task('clean', function(cb) {
    del(['dist/assets/css', 'dist/assets/js', 'dist/assets/img'], cb)
	});
 
	// Default task
	gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'images');
	});
 
	// Watch
	gulp.task('watch', function() {
 
	// Watch .scss files
	gulp.watch('src/styles/**/*.scss', ['styles']);
 
	// Watch .js files
	gulp.watch('src/scripts/**/*.js', ['scripts']);
 
	// Watch image files
	gulp.watch('src/images/**/*', ['images']);
 
	// Create LiveReload server
	livereload.listen();
 
	// Watch any files in dist/, reload on change
	gulp.watch(['dist/**']).on('change', livereload.changed);
 
	});


###Uitleg van de file###




In de code krijgen we een lange lijst met plugins te zien. Deze plugins declareren we hier en gaan we vervolgens laden.
	
	var gulp = require('gulp'),
    	sass = require('gulp-ruby-sass'),
    	autoprefixer = require('gulp-autoprefixer'),
    	minifycss = require('gulp-minify-css'),
    	jshint = require('gulp-jshint'),
    	uglify = require('gulp-uglify'),
    	imagemin = require('gulp-imagemin'),
    	rename = require('gulp-rename'),
    	concat = require('gulp-concat'),
    	notify = require('gulp-notify'),
    	cache = require('gulp-cache'),
    	livereload = require('gulp-livereload'),
    	del = require('del');

Vervolgens gaan we  Sass configureren. We compileren en runnen Sass met Autoprefixer en slagen het op naar onze bestemming. Nadien maken we minified .min version , autorefresh en notify aan. Zo kunnen we veranderingen in onze applicatie zien zonder te refreshen en wanneer een opdracht voltooid is zal deze ook getoond worden.

	gulp.task('styles', function() {
	  return gulp.src('src/styles/main.scss')
    	.pipe(sass({ style: 'expanded' }))
    	.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    	.pipe(gulp.dest('dist/assets/css'))
    	.pipe(rename({suffix: '.min'}))
    	.pipe(minifycss())
    	.pipe(gulp.dest('dist/assets/css'))
    	.pipe(notify({ message: 'Styles task complete' }));
	});


Compressen van images gaat ook en dit vinden we terug in de code. Dit zal alle source images door de imagemin plugin runnen. Compressen betekend een image zo klein mogelijke bestandsgrote geven zonder er een kwaliteitsverlies.

		gulp.task('images', function() {
	return gulp.src('src/images/**/*')
    	.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    	.pipe(gulp.dest('dist/assets/img'))
    	.pipe(notify({ message: 'Images task complete' }));
	});

Nog een leuke tool die gulp ons biedt is de clean.function. Hier gaat men opzoek naar mappen om ze weder op te bouwen wanneer de bron er van verwijderd is.

	gulp.task('clean', function(cb) {
    	del(['dist/assets/css', 'dist/assets/js', 'dist/assets/img'], cb)
	});


Als we specifiek files willen bekijken of ze veranderd zijn, kunnen we dit in de gaten houden via het watch command.

	gulp.task('watch', function() {

	// Watch .scss files
	gulp.watch('src/styles/**/*.scss', ['styles']);

	// Watch .js files
	gulp.watch('src/scripts/**/*.js', ['scripts']);

	// Watch image files
	gulp.watch('src/images/**/*', ['images']);

	});



LiveReload zorgt op zijn beurt dat we niet meer onze pagina moeten refreshen om veranderingen op onze pagina te zien.


	gulp.task('watch', function() {

		// Create LiveReload server
		livereload.listen();

		// Watch any files in dist/, reload on change
		gulp.watch(['dist/**']).on('change', livereload.changed);

	});


### Opstarten van gulp ###

Na onze configuratie willen we gulp uitvoeren dit kan zeer gemakkelijk met behulp van 1 command in de prompt
	
	$gulp



