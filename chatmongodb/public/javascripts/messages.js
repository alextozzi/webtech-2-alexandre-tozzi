$(document).ready(function(){
    var socket = io.connect('http://localhost:3000'),
        user,
        question;

    $('#gebruiker input').keydown(function(e) {
        if (e.keyCode == 13) {
            $('#gebruiker').submit();
        }
    });

    $('#vraag textarea').keydown(function(e) {
        if (e.keyCode == 13) {
            $('#vraag').submit();
        }
    });

    $('#gebruiker').on('submit', function(e){
        user = $('#user').val();
        $('#userh3').html('Gebruiker: ' + user);
        $('#gebruiker').css('display', 'none');
        $('#vraag').css('display', 'block');
        e.preventDefault();
        socket.emit('print');
    });

    $('#vraag').on('submit', function(e){
        question = $('#question').val();

        socket.emit('addQuestion', {"user": user, "question": question});
        e.preventDefault();
        $('#question').val('');
    });

    socket.on('printQuestions', function(questions){
        $('#questions').val('');
        for(var i = 0; i<questions.length; i++){
            $('#questions').prepend('<li><div class="message" data-user="'+questions[i].user+'"><h1>'+questions[i].user+'</h1><p>'+questions[i].question+'</p></div></li>');
            $('#questions li div').filter('[data-user="'+user+'"]').addClass('post');
        }
    });

    socket.on('update', function(update){
        $('#questions').prepend('<li><div class="message new" data-user="'+update.user+'"><h1>'+update.user+'</h1><p>'+update.question+'</p></div></li>');
        $('#questions li div').filter('[data-user="'+user+'"]').addClass('post');
    });
});