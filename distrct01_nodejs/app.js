var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	mongoose = require('mongoose'),
	swig = require('swig'),
	bodyParser = require('body-parser'),
	path = require('path'),
	restful = require('node-restful'),
	io = require('socket.io')(http);

mongoose.connect('mongodb://localhost/imd');

app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/views/index.html');
});

app.get('/test', function(req, res) {
	res.send('Hello World');
});

var Product = app.product = restful.model('Product', mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	amount: {
		type: Number,
		required: true,
		default: 0
	}
}, {collection: 'product'})).methods(['get', 'post', 'put', 'delete']);
Product.register(app, '/product');
io.on('connection', function(socket) {
    console.log('user connected');
    socket.on('disconnect', function() {
        console.log('user disconnected');
    });


        socket.on('product', function(a) {
            Product.create({product: a}, function(err, b) {
                io.emit('addproduct', b);
                console.log(b.name + 'Added Product');
            });
        });
    socket.on('plus', function(a){
        Product.find({name: a}).exec(function(err, amount){
            socket.emit('quantityMore', amount);
        });
    });
    socket.on('min', function(a){
        Product.find({name: a}).exec(function(err, amount){
            socket.emit('quantityLess', amount);
        });
    });

    socket.on('quantity', function(a){
        Product.update({name: a.name}, {amount: a.amount}, function(err, b){

                Product.find().exec(function(err, products){
                    io.sockets.emit('ProductsList', products);
                });

        });
    });
});
io.on('connect', function(socket) {
    Product.find()
        .exec(function(err, products) {
            socket.emit('ProductsList', products);
        });
});




var server = http.listen(3003, function(){
	console.log('Server running on http://localhost:3003');
});

module.exports = app;